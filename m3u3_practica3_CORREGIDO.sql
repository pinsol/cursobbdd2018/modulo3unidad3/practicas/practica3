﻿                                           /* Practica 3 */

DROP DATABASE IF EXISTS practica3apoyo;
CREATE DATABASE practica3apoyo;
USE practica3apoyo;

-- Ejercicio 1.
CREATE OR REPLACE TABLE ZonaUrbana(
  NombreZona int AUTO_INCREMENT,
  Categoría char(1),
  PRIMARY KEY(NombreZona)
  );

CREATE OR REPLACE TABLE BloqueCasas(
  Calle int AUTO_INCREMENT,
  Número int,
  Npisos int,
  NombreZona varchar(20),
  PRIMARY KEY(Calle, Número)
  );

CREATE OR REPLACE TABLE CasaParticular(
  NombreZona int,
  Número int,
  Metros int,
  PRIMARY KEY(NombreZona, Número)
  );

CREATE OR REPLACE TABLE Piso(
  Calle int,
  Número int,
  Planta int,
  Puerta int,
  Metros int,
  PRIMARY KEY(Calle, Número, Planta, Puerta)
  );

CREATE OR REPLACE TABLE Persona(
  DNI char(9),
  Nombre varchar(10),
  Edad int,
  NombreZona int,
  NumCasa int,
  Planta int,
  Puerta int,
  PRIMARY KEY(DNI)
  );

CREATE OR REPLACE TABLE PoseeC(
  DNI char(9),
  Calle int,
  NumBloque int,
  Planta int,
  Puerta int,
  PRIMARY KEY(DNI, Calle, NumBloque, Planta, Puerta)
  );

-- Ejercicio 2. Procedimiento que te dice si un valor se encuentra en la tabla.
INSERT INTO ZonaUrbana(NombreZona) VALUES ('105'),('55'),('619');
SELECT * FROM ZonaUrbana;

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2(IN arg1 int)
  BEGIN
    IF arg1 IN (SELECT
          NombreZona
        FROM ZonaUrbana) THEN
      SELECT 'El valor se encuentra en la tabla';
    ELSE
      SELECT 'El valor no se encuentra en la tabla';
    END IF;

  /* SELECT nombrezona, LOCATE(arg1, NombreZona) FROM ZonaUrbana; */  -- solo indica la posicion


  END//
DELIMITER ;

CALL ej2(1);

-- solucion ramon
  DELIMITER //
CREATE OR REPLACE PROCEDURE ejercicio2 (arg int)
BEGIN
  DECLARE v1 int; -- variable para cursor
  -- variable para contar registros e imprimir el numero de registro donde encuentre nuestro valor
  DECLARE r int DEFAULT 1;
  -- variable  cuando encuentre el valor o cuando llegue el final de la tabla a recorrer
  DECLARE llave int DEFAULT 1;
  -- cursor para movvermoe por la tabla
  DECLARE c1 CURSOR FOR
  SELECT zu.NombreZona 
  FROM ZonaUrbana zu;

  -- control excepciones para

  OPEN c1;
  REPEAT
    FETCH c1 INTO v1;
    
    IF (v1=arg) THEN
      SELECT
        CONCAT('Valor encontrado en registro',r) salida;
      SET llave=0;
    /* else 
        select concat('no encontrado ',r) salida; */
      ELSE
        SELECT
          CONCAT('Valor no encontrado en registro',r) salida;
        set llave=1;
    END IF;
      SET r = r+1;
    UNTIL (llave <> 1)
      END REPEAT;
  CLOSE c1;
  end //
  DELIMITER ;

-- Ejercicio 3. Procedimiento que introduzca los valores en la tabla zona_urbana.

DELIMITER //
CREATE OR REPLACE PROCEDURE ej3(IN nombrezona int, IN categoría char(1))
  BEGIN
  INSERT INTO ZonaUrbana VALUES
  (nombrezona, categoría);
  END //
DELIMITER ;

CALL ej3(555, 4);
SELECT * FROM ZonaUrbana;

-- Ejercicio 4. Funcion cuenta registros de la tabla ZonaUrbana

DELIMITER //
CREATE OR REPLACE FUNCTION f3()
  RETURNS int
  COMMENT 'Esta función cuenta el numero de registros que hay en la tabla ZonaUrbana'
  BEGIN
  DECLARE v1 int;
  set v1= (SELECT COUNT(*) FROM ZonaUrbana);
  RETURN v1;
  END //
  DELIMITER ;

SELECT f3();

-- Ejercicio 5. Procedimiento que emplea f3 e introduce el valor en casaparticular siempre que haya algun registro

DELIMITER //
CREATE OR REPLACE PROCEDURE ej5(nombreZona int, numero_entrada int, metros int)
  BEGIN
    DECLARE numero int DEFAULT 0;

    SET numero=f3();

     -- si no hay zonas no puedo hacer nada
    IF numero > 0 THEN 
      SELECT COUNT(*) INTO numero FROM
        ZonaUrbana z
        WHERE z.NombreZona=nombreZona;
    -- la zona urbana existe?
    IF numero = 1 THEN 
      INSERT INTO CasaParticular
        VALUE (nombreZona,numero_entrada,metros);
    ELSE /*
      -- mensaje a pantalla
      SELECT 'no existe la zona urbana';
      
      -- mensaje a canal de errores */
      SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT='no existe esa zona urbana';
    END IF;
    ELSE/*
      -- mensaje a pantalla
      SELECT 'no hay zonas urbanas';*/
      -- mensaje a canal de errores
      SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT='no hay zonas urbanas';
    END IF;    
  END //
DELIMITER ;

CALL ej5(641,6,120);

SELECT * FROM casaparticular;
SELECT * FROM ZonaUrbana zu;

/* solucion de Ramon --donde esta el error? suma los registros de 6,12,24
  -- ah! pos no! error por tener PK xq son los dos primeros campos
  -- otro error es que en cada bucle tarda en guardar el dato, es recomendable darle un tiempo o usar REPEAT
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE p5 ()
BEGIN

  -- creando variables
  DECLARE v1 int;

  DECLARE i int DEFAULT 1;
  -- DECLARE llave int DEFAULT 1;

  -- creo un cursor para la tabla zona urbana
  DECLARE c1 CURSOR FOR
    SELECT
      zu.NombreZona
    FROM ZonaUrbana zu;

  -- control de salida para el cursor
  /*DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET llave = 0;*/

    -- control error de PK duplicada
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
      SET v1=i-1;
  
  -- abrimos el cursor
  OPEN c1;
  WHILE (i <= f3()) DO
    -- leo el primer registro 
    -- almaceno en v1 la primera zona urbana
    FETCH c1 INTO v1;
    -- introduzco el registro en casaParticular
    INSERT INTO CasaParticular
      VALUES (v1, ROUND(RAND() * 100), ROUND(RAND() * 300) + 200);

     -- visor
          select v1,i;  -- ademas de permitir echar un vistazo nos crea un tiempo NECESARIO en el almacenamiento del dato

    -- sumar 1 al contador
    SET i = i + 1;

  END WHILE;
  -- cierro el cursor
  CLOSE c1;
  SELECT
    CONCAT('Ha insertado ', f3(), ' registros.') salida;
END //
DELIMITER ;

SELECT * FROM zonaurbana z;
CALL p2('a');
SELECT * FROM casaparticular c;
DELETE FROM casaparticular;
CALL p5();
SELECT f3();




-- Ejercicio 6. Crear vista que muestre todos los campos de zona urbana con los campos numero y metros de casaparticular
CREATE OR REPLACE VIEW ej6 AS
  SELECT
   ZonaUrbana.*,CasaParticular.Número,CasaParticular.Metros 
  FROM
   ZonaUrbana JOIN CasaParticular USING(NombreZona)
  ;

SELECT *  FROM ej6;

-- Ejercicio 7. Procedimiento que introduzca de forma automatica 100 registros en ZonaUrbana (usar RAND() y ROUND(num))

DELIMITER //
CREATE OR REPLACE PROCEDURE p6()
  BEGIN
    DECLARE temp int DEFAULT 0;
    DECLARE error int DEFAULT 0;   -- para controlar errores
    DECLARE CONTINUE HANDLER FOR 1062  -- para controlar errores
      SET error=1;

    WHILE (temp<=100) DO
      -- intento introducir el registro
        INSERT INTO ZonaUrbana(NombreZona)
          VALUES (ROUND(RAND()*1000000));
      -- ha entrado??       CONTROL DE ERRORES
      IF(error=0) THEN
        SET temp=temp+1;
      ELSE
        SET error=0;
      END IF;      
    END WHILE;

  END //
DELIMITER ;

CALL p6();
SELECT * FROM ZonaUrbana;


/* Ejercicio 8. Procedimiento que introduzca 100 registros en CasaParticular.
                Crear funcion que seleccione un valor del campo NombreZona de la tabla ZonaUrbana */

DELIMITER //
CREATE OR REPLACE FUNCTION f4()
  RETURNS int
  COMMENT 'Esta función selecciona un valor aleatorio del campo NombreZona de la tabla ZonaUrbana'
  BEGIN
    DECLARE nRegistros int;
    DECLARE valor int;
    DECLARE resultado int;

      set nRegistros=(SELECT COUNT(*) FROM ZonaUrbana);

      set valor=(RAND()*nRegistros);

      SELECT z.NombreZona INTO resultado FROM ZonaUrbana z LIMIT valor,1; 

  RETURN resultado;

  END //
DELIMITER ;

SELECT f4();


DELIMITER //
CREATE OR REPLACE PROCEDURE p7()
  BEGIN
    DECLARE temp int DEFAULT 0;
    ALTER TABLE ZonaUrbana ADD COLUMN IF NOT EXISTS (ID int);

      WHILE (temp<=101) DO
        SET temp=temp+1;
        INSERT INTO ZonaUrbana(ID) VALUES (f4());
      END WHILE;

  END //
DELIMITER ;

CALL p7();
SELECT * FROM ZonaUrbana;


/* Ejercicio 9: Crear un procedimiento que utilizando un cursor te muestre los registros del 1 al 10 */
DELIMITER //
CREATE OR REPLACE PROCEDURE p8()
BEGIN
  DECLARE v1 int;
  DECLARE incr int DEFAULT 0;
  DECLARE v2 char(2);
  DECLARE c1 CURSOR FOR

  SELECT z.NombreZona,
         z.Categoría FROM ZonaUrbana z;

  OPEN c1;

  WHILE (incr < 10) DO
    FETCH c1 INTO v1, v2;
    SELECT v1 NombreZona, v2 Categoria;

    SET incr = incr + 1;
  END WHILE;
  CLOSE c1;
END //
DELIMITER ;

CALL p8();

/* Ejercicio 10: Introducir datos utilizando un script en la tabla bloque casas */
SELECT * FROM BloqueCasas;

INSERT INTO bloquecasas VALUES
('C/ Principal', 4, 5, 20),
('C/ Principal', 7, 4, 66),
('C/ Principal', 9, 9, 30),
('C/ Principal', 16, 5, 85),
('C/ Principal', 33, 5, 32),
('C/ Principal', 45, 2, 42),
('C/ Principal', 67, 7, 51),
('C/ Principal', 83, 6, 66),
('C/ Principal', 90, 7, 81),
('C/ Principal', 99, 9, 83);

SELECT * FROM BloqueCasas;

/* Ejercicio 11: Crear un procedimiento, llamarlo p10, donde podamos introducir datos de forma automática
                 en la tabla piso. Tener en cuenta que los campos numero y calle tenéis que sacarlos de la
                 tabla bloque casas. Introducir 23 registros. */
DELIMITER //
CREATE OR REPLACE PROCEDURE p10()
BEGIN
  DECLARE calle int;
  DECLARE piso int;
  DECLARE numpisos int;
  DECLARE incr int DEFAULT 0;
  DECLARE v1 int;
  DECLARE n int;
  DECLARE error int DEFAULT 0;

  DECLARE CONTINUE HANDLER FOR 1062
    SET error=1;

    SET v1=(SELECT COUNT(*) FROM BloqueCasas bc);

    

  WHILE (incr<23) DO
    SET n=(ROUND(RAND()*v1));

    SELECT
      bc.Calle,
      bc.Número,
      bc.Npisos INTO calle, piso, numpisos
    FROM BloqueCasas bc LIMIT n,1;

    INSERT INTO Piso
      VALUES (calle, piso, ROUND(RAND()*numpisos)+1, ROUND(RAND()*6)+50, ROUND(RAND()*400)+200);
     IF (error=1) THEN 
      SET error=0;
     ELSE 
      SET incr=incr+1; 
     END IF;
  END WHILE;

END //
DELIMITER ;

CALL p10();

SELECT * FROM BloqueCasas bc;
SELECT * FROM piso p;
DELETE FROM piso;

/* Ejercicio 12: Crear un procedimiento al cual le pasas una fecha en el primer argumento y
                 que te retorna tres argumentos (lógicamente de salida) con el día,
                 mes y año de la fecha generada */
DELIMITER //
CREATE OR REPLACE PROCEDURE p11(IN fecha date, OUT dia int, OUT mes int, OUT anho int)
BEGIN
  SET dia=DAY(fecha);
  SET mes=MONTH(fecha);
  SET anho=YEAR(fecha);

  SELECT dia;
  SELECT mes;
  SELECT anho;

END //
DELIMITER ;


CALL p11('2017-11-10',@dia,@mes,@anho);

/* Ejercicio 13: Crear una función a la cual le pasas una fecha y te devuelve el numero de años
                 que tiene esa persona teniendo en cuenta la fecha actual */
DELIMITER //
CREATE OR REPLACE FUNCTION f5(fecha date)
RETURNS int
BEGIN
  DECLARE anhos int DEFAULT 0;

  SET anhos=YEAR(NOW())-YEAR(fecha);

  IF (MONTH(NOW())<MONTH(fecha)) THEN
    SET anhos=anhos-1;
  ELSEIF (MONTH(NOW())=MONTH(fecha)) THEN
    IF (DAY(NOW())<DAY(fecha)) THEN
      SET anhos=anhos-1;
    END IF;
  END IF;
  RETURN anhos;
END //
DELIMITER ;

SELECT f5('2017/10/11');

/* Ejercicio 14: Crear una función a la cual le pasas un nombre y te devuelve
                 el nombre sin espacios en blanco en la derecha y en la izquierda
                 (utilizar las funciones RTRIM y LTRIM) */
DELIMITER //
CREATE OR REPLACE FUNCTION f6(nombre varchar(30))
RETURNS varchar(30)
BEGIN
  DECLARE juntar varchar(30);

  set juntar=RTRIM(LTRIM(nombre));

  RETURN juntar;
END //
DELIMITER ;

SELECT f6('   Pepe');